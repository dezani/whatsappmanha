app.controller('UsuarioListaCtrl',
     function($scope, $state) { // ~ class

    $scope.usuarios = [
        {
            'id' : 1,
            'nome' : 'Batman',
            'email' : 'batman@erobin.com',
            'status' : true,
            'endereco' : {
                'cidade' : 'Gothan City',
                'numero' : 1000
            }
        },
        {
            'id' : 2,
            'nome' : 'Lanterna Verde',
            'email' : 'lanterna@azul.com',
            'status' : true,
            'endereco' : {
                'cidade' : 'Coastal City',
                'numero' : 3
            }
        }
    ]

    $scope.newUser = function() {
        $state.go('usuario-create');
    }
}); 
